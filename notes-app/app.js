const chalk = require('chalk')
const yargs = require('yargs')
const notes = require('./notes.js')

// Customize yargs version
yargs.version('1.1.0')

// Create Add command
yargs.command({
    command: 'add',
    describe: 'Add a new note',
    builder: {
        title: {
            describe: 'Note title',
            demandOption: true,
            type: 'string'
        },
        body: {
            describe: 'Note Body',
            demandOption: true,
            type: 'string'
        }
    },
    handler: (argv) => notes.addNotes(argv.title, argv.body)
})

// Create Remove command
yargs.command({
    command: 'remove',
    describe: 'Remove a note by title.',
    builder: {
        title: {
            describe: 'Note title',
            demandOption: true,
            type: 'string'
        },    
    },
    handler: (argv) => notes.removeNotes(argv.title)
})

// Create Read command
yargs.command({
    command: 'read',
    describe: 'Read a note...',
    builder: {
        title: {
            describe: 'Note title',
            demandOption: true,
            type: 'string'
        },    
    },
    handler: (argv) => notes.readNotes(argv.title)
})

// Create List command
yargs.command({
    command: 'list',
    describe: 'List all notes...',
    handler: () => notes.listNotes()
})

yargs.parse()
