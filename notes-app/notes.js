const chalk = require('chalk')
const fs = require('fs')
const getNotes = (note) => note

const addNotes = (title, body) => {
    const notes = loadNotes()
    const duplicateNote = notes.find((note) => note.title === title)

    if (!duplicateNote) {
        notes.push({
            title: title,
            body: body
        })
        saveNotes(notes)
        console.log(chalk.green.bold('New note added.'))
    } else {
        console.log(chalk.red.bold('Note title already exists.'))
    }
}

const removeNotes = (title) => {
    const notes = loadNotes()
    const notesToKeep = notes.filter((note) => note.title !== title)

    notes.length > notesToKeep.length ? console.log(chalk.green.bold('Note is sucessfully removed!')) : console.log(chalk.red.bold('No note found!'))
    
    saveNotes(notesToKeep)
}

const listNotes = () => {
    const notes = loadNotes()
    
    notes.forEach(note => {
        console.log(chalk.green.bold('\nTitle: ' + note.title)),
        console.log(chalk.green.bold('\nBody: ' + note.body))
    });
}

const readNotes = (title) => {
    const notes = loadNotes()
    const notesToRead = notes.find((note) => note.title === title)
    
    if (notesToRead) {
        console.log(chalk.green.bold('\nTitle: ' + notesToRead.title)),
        console.log(chalk.green.bold('\nBody: ' + notesToRead.body))
    } else {
        console.log(chalk.red.bold('Note title not found.'))
    }
}

const saveNotes = (notes) => {
    const dataJSON = JSON.stringify(notes)
    fs.writeFileSync('notes.json', dataJSON)
}

const loadNotes = () => {
    try {
        const dataBuffer = fs.readFileSync('notes.json')
        const dataJSON = dataBuffer.toString()
        return JSON.parse(dataJSON)
    } catch (e) {
        return []
    }
}

module.exports = {
    getNotes: getNotes,
    addNotes: addNotes,
    removeNotes: removeNotes,
    listNotes: listNotes,
    readNotes: readNotes
} 