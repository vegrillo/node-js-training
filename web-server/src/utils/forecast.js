const request = require('postman-request');

const forecast = (latitude, longitude, callback) => {
  const url =
    'http://api.weatherstack.com/current?access_key=6aeea6448611746b910af03769e5c296&query=' +
    latitude +
    ',' +
    longitude +
    '&units=m';

  request({ url, json: true }, (error, { body }) => {
    debugger;
    if (error) {
      callback('Unable to connect to location services !', undefined);
    } else if (body.error) {
      callback('Unable 1123to find a location.');
    } else {
      callback(
        undefined,
        body.current.weather_descriptions[0] +
          ' it is currently ' +
          body.current.temperature +
          ' degress out. It feels like ' +
          body.current.feelslike +
          ' degrees out.'
      );
    }
  });
};

module.exports = forecast;
