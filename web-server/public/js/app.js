console.log('Client side javascript file is loaded!');

const weatherForm = document.querySelector('form');
const search = document.querySelector('input');
const messageOne = document.querySelector('#first-message');
const messageTwo = document.querySelector('#second-message');

search.addEventListener('change', (e) => {
  e.preventDefault();
  
  messageOne.innerHTML = '<div class="loader"><div class="inner one"></div><div class="inner two"></div><div class="inner three"></div></div>';
  messageTwo.innerHTML = '<p id="second-message"></p>';
})


weatherForm.addEventListener('submit', (e) => {
  e.preventDefault();

  const location = search.value;
  
  fetch('http://localhost:3000/weather?address='+location).then((response) => {
    response.json().then((data) => {
      if (data.error) {
        return messageOne.textContent = data.error;
      }else {
        messageOne.textContent = data.location;
        messageTwo.textContent = data.forecast;
      }    

    })
  })

})