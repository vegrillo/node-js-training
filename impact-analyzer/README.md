#Impact Analyzer

A command-line tool that checks if a CSS selector finds any results on all the site's pages


## How to install
First, you need to install globally PhantomJs and CasperJs globally
```sh
$ npm -g i phantomjs casperjs
```

Next, from the tool's root directory, install all the dependencies from node.
```sh
$ npm i
```

And that's it!

----------------------
Command Line Arguments
----------------------
--url | -u  
  (required) The base url to check, it requires a well format url.
  It accepts the base auth data using the format: http://username:pass@www.example.com

--selector | -s
  (required) The CSS selector to check for
  Needs to be wrapped in quotation marks

--quiet | -q
  Runs the script in quiet mode, avoiding the printing of unnecessary message.
  In other words, it means that the script will only output Matching URL's.

## How to use
From the tool's root directory.
```sh
$ ./test.js --url "https://user:pass@example.com" --selector "body .class #id"
```
