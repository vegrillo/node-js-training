#!/usr/bin/env node
var url = require('url');
var request = require('request');
var exec = require('child_process').exec;
var yargs = require('yargs')
    .usage('Usage: ./$0 --url "https://user:pass@example.com" --selector "body .class #id"')
    .options({
      'url' : {
        alias: 'u',
        describe: "The base url to check",
        demand: true,
        type: 'string',
        requiresArg : true
      },
      'selector' : {
        alias: 's',
        describe: 'The selector to check for',
        demand: true,
        type: 'string',
        requiresArg : true
      },
      'quiet' : {
        alias: 'q',
        describe: 'Output only url\'s that match and/or errors.',
        demand: false,
        type: 'boolean',
        requiresArg : false,
      },
    });
var jsdom = require("jsdom");
var argv = yargs.argv
var baseurl = argv.url;
var urls = [];

// Maximum jobs that can run in parallel.
const MAX = 20;

//Helper function to validate the input url
function validateUrl(url) {
  var url_regexp = new RegExp(/^(https?:)(\/\/([\w]*(?::[\w]*)?@)?([\d\w\.-]+)(?::(\d+))?)?([\/\\\w\.()-]*)?(?:([?][^#]*)?(#.*)?)$/igm);
  if (url.match(url_regexp)) {
    return url;
  }
  else {
    throw "Unvalid URL format: " + url;
  }
}

// checkCallBack Function
function checkCallback(err, stdout, stderr) {
  if (typeof stdout !== 'undefined') {
    if (!argv.quiet || !stdout.includes("[No Match!]")) {
      console.log(stdout.replace(/[\n\r]/g, ''));
    }
  }
  if (urls.length <= 0) {
    return;
  }
  var url = urls.shift();
  var command = [
    'casperjs',
    './includes/check-url.js',
    url,
    '"' + argv.selector + '"'
  ];
  exec(command.join(' '), checkCallback);
}

// Validate the input
yargs.check(function(argv) {
  validateUrl(argv.url);
  return true;
});

var xml2js = require('xml2js');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = 1

var sitemap = url.parse(baseurl);
sitemap.path = '/sitemap.xml';

if (!argv.quiet) {
  console.log("Checking", argv.url, "for the CSS selector: ", argv.selector);
}

request({url: sitemap}, function(err, res, body) {
  if (err) {
    throw "Error loading XML: " + err.message;
  } else {
    var parser = new xml2js.Parser();
    // Parse the body string as XML.
    parser.parseString(body, function(err, result) {
      if (err) {
        throw "Error parsing XML: " + err.message;
      } else if (typeof result.urlset != 'undefined') {
        var casperscript = 'includes/check-url.js';
        for( var i in result.urlset.url) {
          var sitemap_entry = argv.url + url.parse(result.urlset.url[i].loc[0]).path;
          urls.push(sitemap_entry);
        }
        for (i = 0; i < MAX; i++) {
          checkCallback();
        }
      } else {
        // XML returned is not well formed
        throw "Error parsing XML";
      }
    });
  }
});
