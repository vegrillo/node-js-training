var casper = require('casper').create({
  // clientScripts: ['jquery.min.js']
});

if (!casper.cli.has(0)) {
  casper.log('Please pass an URL to the Script.');
  casper.exit(1);
}
var url = casper.cli.get(0);

if (!casper.cli.has(1)) {
  casper.log('Please pass an selector to the Script.');
  casper.exit(1);
}
var selector = casper.cli.get(1);

casper.start(url, function () {
  var count = this.evaluate(function(selector) {
    return jQuery(selector).length;
  }, selector);
  var statusCode = 2;
  if (count > 0) {
    this.echo('\033[0;32m[Match!]\033[0m The URL ' + url + ' has ' + count + ' matches of the CSS selector');
    statusCode = 0;
  }
  else {
    this.echo('\033[0;33m[No Match!]\033[0m The URL ' + url + ' doesn\'t have any matches of the CSS selector');
  }
  casper.exit(statusCode);
});

casper.run();
