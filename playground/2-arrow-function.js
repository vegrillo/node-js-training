// const square = function (x) {
//     return x * x
// }

// const square = (x) => {
//     return x * x
// }

// const square = (x) => x * x

const event = {
    name: 'Birthday Party',
    guestList: ['Vitor', 'Maisa', 'Will'],
    printGuestList() {
        console.log('Guest list for ' + this.name)

        // Binding method is not possible without arrow functions for this scope.
        // this.guestList.forEach( function (guest) {
        //     console.log(guest + ' is attending ' + this.name)
        // })
        this.guestList.forEach((guest) => {
            console.log(guest + ' is attending ' + this.name)
        })
    }
}

// console.log(square(3))
event.printGuestList()